all: report.pdf

report.pdf: report.tex
	latexmk -pdf

clean:
	latexmk -C

.PHONY: all clean
